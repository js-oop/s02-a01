//FUNCTION CODING:

//Translate the other students specifics into their own respective objects. Make sure to include the login, logout, and listGrades method in each object, as well.


let studentOne = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        return avg
    },
    willPass(){
        if(this.gradeAverage() >= 85){
            return "true"
        }
        else{
            return "false"
        }      
    },
    willPassWithHonors(){
        if(this.gradeAverage() >= 90){
            return "true"
        } if(this.gradeAverage() >= 85 && this.gradeAverage() < 90){
            return "false"
        } else{
            return undefined
        }      
    }
}

let studentTwo = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        return avg
    },
    willPass(){
        if(this.gradeAverage() >= 85){
            return "true"
        }
        else{
            return "false"
        }      
    },
    willPassWithHonors(){
        if(this.gradeAverage() >= 90){
            return "true"
        } if(this.gradeAverage() >= 85 && this.gradeAverage() < 90){
            return "false"
        } else{
            return undefined
        }      
    }
}

let studentThree = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        return avg
    },
    willPass(){
        if(this.gradeAverage() >= 85){
            return "true"
        }
        else{
            return "false"
        }      
    },
    willPassWithHonors(){
        if(this.gradeAverage() >= 90){
            return "true"
        } if(this.gradeAverage() >= 85 && this.gradeAverage() < 90){
            return "false"
        } else{
            return undefined
        }      
    }
}

let studentFour = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
    },
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        return avg
    },
    willPass(){
        if(this.gradeAverage() >= 85){
            return "true"
        }
        else{
            return "false"
        }      
    },
    willPassWithHonors(){
        if(this.gradeAverage() >= 90){
            return "true"
        } if(this.gradeAverage() >= 85 && this.gradeAverage() < 90){
            return "false"
        } else{
            return undefined
        }      
    }
}

let classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents(){
        let x = 0
        let i = 0
        

        do{
            if(this.students[x].willPassWithHonors() == "true"){
                i++;
                x++;
            } else{x++}
            
        }
        while (x < this.students.length);

        return i
    },
    honorsPercentage(){
        let percent = this.countHonorStudents() / (this.students.length) * 100
        return percent + "%"
    },
    retrieveHonorStudentInfo(){
        let x = 0
        let i = 0
        let honors = []
        

        do{
            if(this.students[x].willPassWithHonors() == "true"){
                honors.push(this.students[x])
                i++;
                x++;
            } else{x++}
            
        }
        while (x < this.students.length);

        return honors
    },
    sortHonorStudentsByGradeDesc(){
        let x = 0
        let i = 0
        let honors = []
        

        do{
            if(this.students[x].willPassWithHonors() == "true"){
                honors.push(this.students[x])
                i++;
                x++;
            } else{x++}
            
        }
        while (x < this.students.length);

        honors.sort((a, b) => {
            return b.gradeAverage() - a.gradeAverage();
        });
        
        return honors
    }
}



//Define a method for EACH student object that will compute for their grade average (total of grades divided by 4).


//Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.


//Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).


//Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.


//Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.


//Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.


//Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.


//Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
